class ReadFile():
    def __init__(self, file_path):
        self.file_path = file_path
        
    def read_file(self):
        file = []
        with open(self.file_path) as file_reader:
            for line in file_reader:
                file.append(line.split())
        return file
  