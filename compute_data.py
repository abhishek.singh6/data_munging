import re

class ComputeData():
    def __init__(self, file, value_key, value_1, value_2):
        self.file = file
        self.value_key = value_key
        self.value_1 = value_1
        self.value_2 = value_2

    def extract_data(self):
        count = 0
        extracted_data = []
        for line in self.file:
            if len(line) > 1 and count > 0:
                temp_line = []
                temp_line.append(line[self.value_key])
                temp_line.append(re.sub(r"\D", "", line[self.value_1]))
                temp_line.append(re.sub(r"\D", "", line[self.value_2])) 
                extracted_data.append(temp_line)
            count += 1
        return extracted_data     
    
    def compute_data(self):
        extracted_data = self.extract_data()
        max_difference = abs(int(extracted_data[0][2])-int(extracted_data[0][1]))
        
        for line in extracted_data:
            value_difference = abs(int(line[1])-int(line[2]))
            if value_difference < max_difference:
                result_key = line[0]
                result_value = value_difference
                max_difference = value_difference
        return result_key, result_value
        
        
    
        
        