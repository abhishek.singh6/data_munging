from read_file import ReadFile
from compute_data import ComputeData

class FootballExe():
    def __init__(self, file_name, value_key, value_1, value_2):
        self.file_name = file_name
        self.value_key = value_key
        self.value_1 = value_1
        self.value_2 = value_2
    
    def execute_footall(self):
        file_object = ReadFile(self.file_name)
        file = file_object.read_file()
        computed_data_object = ComputeData(file, self.value_key, self.value_1, self.value_2)
        result = computed_data_object.compute_data()
        return result
     
footballexe = FootballExe("football.dat", 1, 6, 8)     
print(footballexe.execute_footall())   
        